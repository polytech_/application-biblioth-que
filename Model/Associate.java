package BiblioMVC.Model;

public class Associate {

    private int id;
    private String nomComplet;
    private double dureeMoy;


    public Associate(int id, String nomComplet, double dureeMoy) {
        this.id = id;
        this.nomComplet = nomComplet;
        this.dureeMoy =  dureeMoy;
    }

    public double getDureeMoy() {
        return dureeMoy;
    }

    public void setDureeMoy(double dureeMoy) {
        this.dureeMoy = dureeMoy;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomComplet() {
        return nomComplet;
    }

    public void setNomComplet(String nomComplet) {
        this.nomComplet = nomComplet;
    }
}
