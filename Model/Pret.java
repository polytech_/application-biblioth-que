package BiblioMVC.Model;

import java.text.ParseException;
import java.time.LocalDate;


public class Pret {
    private int livre;
    private int associate;
    private LocalDate dateEmprunt;
    private LocalDate dateRetour;


    public Pret(int livre, int associate, String date_emprunt, String date_retour) throws ParseException {
        this.livre = livre;
        this.associate = associate;
        this.dateEmprunt  = LocalDate.parse(date_emprunt);
        this.dateRetour  = LocalDate.parse(date_retour);
    }


    public int getLivre() {
        return livre;
    }

    public void setLivre(int livre) {
        this.livre = livre;
    }

    public int getAssociate() {
        return associate;
    }

    public void setAssociate(int associate) {
        this.associate = associate;
    }

    public LocalDate getDate_emprunt() {
        return dateEmprunt;
    }

    public void setDate_emprunt(LocalDate dateParam) {
        this.dateEmprunt = dateParam;
    }

    public LocalDate getDate_retour() {
        return dateRetour;
    }

    public void setDate_retour(LocalDate dateParam) {
        this.dateRetour = dateParam;
    }
}
