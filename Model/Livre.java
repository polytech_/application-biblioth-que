package BiblioMVC.Model;

public class Livre {

    private int ISBN;
    private String titre;
    private int copies;
    private int prets;
    private int Exemplaires;

    public Livre() {
    }

    public Livre(int ISBN, int copies, String titre, int prets, int Exemplaire) {
        this.ISBN = ISBN;
        this.titre = titre;
        this.copies = copies;
        this.prets = prets;
        this.Exemplaires = Exemplaire;
    }

    public Livre(String titre, int nbCopies) {
        this.titre = titre;
        this.copies = nbCopies;
    }

    public int getISBN() {
        return ISBN;
    }
    public void setISBN(int ISBN) {
        this.ISBN = ISBN;
    }

    public String getTitre() {
        return titre;
    }
    public void setTitre(String titre) {
        this.titre = titre;
    }

    public int getCopies() {
        return copies;
    }
    public void setCopies(int copies) {
        this.copies = copies;
    }

    public int getPrets() { return prets; }
    public void setPrets(int prets) {
        this.prets = prets;
    }

    public int getExemplaires() {
        return Exemplaires;
    }
    public void setExemplaires(int Exemplaires) {
        this.Exemplaires = Exemplaires;
    }
}
