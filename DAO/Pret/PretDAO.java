package BiblioMVC.DAO.Pret;

import BiblioMVC.Model.Associate;
import BiblioMVC.Model.Pret;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

public interface PretDAO {

    public List<Pret> listerPrets() throws SQLException, ParseException;


}
