package BiblioMVC.DAO.Pret;

import BiblioMVC.Model.Pret;
import BiblioMVC.DAO.ProcessingDB;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class PretDAOImpl implements PretDAO {

    private ProcessingDB processingDB;

    public PretDAOImpl(ProcessingDB processingDB) {
        this.processingDB = processingDB;
    }

    @Override
    public List<Pret> listerPrets() throws SQLException, ParseException {

        ArrayList<Pret> listPrets = new ArrayList<>();

        ResultSet resultSet = processingDB.getConnection().createStatement().executeQuery("SELECT * FROM prets");

        while(resultSet.next()) {
            listPrets.add(new Pret(resultSet.getInt("Livre"),
                    resultSet.getInt("Associate"),
                    resultSet.getString("date_emprunt"),
                    resultSet.getString("date_retour")));
        }

        return listPrets;

    }
}
