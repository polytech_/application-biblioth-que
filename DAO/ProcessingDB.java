package BiblioMVC.DAO;

import BiblioMVC.DAO.Associe.AssociateDAO;
import BiblioMVC.DAO.Associe.AssociateDAOImpl;
import BiblioMVC.DAO.Livre.LivreDAO;
import BiblioMVC.DAO.Livre.LivreDAOImpl;
import BiblioMVC.DAO.Pret.PretDAO;
import BiblioMVC.DAO.Pret.PretDAOImpl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ProcessingDB {

    private String url;
    private String username;
    private String password;


    public ProcessingDB(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    public static ProcessingDB getInstance() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex1) {
            System.out.println("Pilote non trouvé !");
            System.exit(1);
        }
        return new ProcessingDB("jdbc:mysql://localhost/biblio?serverTimezone=UTC", "othmane", "Aqwzsxedc123");
    }

    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url, username, password);
    }

    //Récupération des DAO
    public PretDAO getPretDAO() {
        return new PretDAOImpl(this);
    }

    public AssociateDAO getAssociateDAO() {
        return new AssociateDAOImpl(this);
    }

    public LivreDAO getLivreDAO() {
        return new LivreDAOImpl(this);
    }

}
