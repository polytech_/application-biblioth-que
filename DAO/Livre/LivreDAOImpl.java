package BiblioMVC.DAO.Livre;

import BiblioMVC.DAO.ProcessingDB;
import BiblioMVC.Model.Livre;
import com.mysql.cj.protocol.Resultset;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LivreDAOImpl implements LivreDAO{

    private ProcessingDB processingDB;

    public LivreDAOImpl(ProcessingDB processingDB) {
        this.processingDB = processingDB;
    }

    @Override
    public List<Livre> listerLivres() throws SQLException {

        ResultSet resultSet = null;

        ArrayList<Livre> listLivres = new ArrayList<>();

        resultSet = processingDB.getConnection().createStatement().executeQuery("SELECT * FROM livre");

        while(resultSet.next()) {
            listLivres.add(new Livre(resultSet.getInt("ISBN"),
                    resultSet.getInt("Copies"),
                    resultSet.getString("Titre"),
                    resultSet.getInt("Prets"),
                    resultSet.getInt("Exemplaires")));
        }

        return listLivres;

    }

    @Override
    public void ajouterLivre(Livre livre) throws SQLException {

        PreparedStatement preparedStatement = null;

        preparedStatement = processingDB.getConnection().prepareStatement("INSERT INTO livre(copies, titre, exemplaires) VALUES(?,?,?)");

        preparedStatement.setInt(1, livre.getCopies());
        preparedStatement.setString(2, livre.getTitre());
        preparedStatement.setInt(3, livre.getExemplaires());

        preparedStatement.executeUpdate();

        preparedStatement.close();

    }
}
