package BiblioMVC.DAO.Livre;

import BiblioMVC.Model.Livre;

import java.sql.SQLException;
import java.util.List;

public interface LivreDAO {

    List<Livre> listerLivres() throws SQLException;

    void ajouterLivre(Livre livre) throws SQLException;
}
