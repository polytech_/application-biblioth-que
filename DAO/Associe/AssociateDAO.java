package BiblioMVC.DAO.Associe;

import BiblioMVC.Model.Associate;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

public interface AssociateDAO {

    public List<Associate> listerAssocies() throws SQLException, ParseException;

}
