package BiblioMVC.DAO.Associe;

import BiblioMVC.Model.Associate;
import BiblioMVC.DAO.ProcessingDB;
import BiblioMVC.Model.Pret;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class AssociateDAOImpl implements AssociateDAO {

    private ProcessingDB processingDB;

    public AssociateDAOImpl(ProcessingDB processingDB) {
        this.processingDB = processingDB;
    }


    @Override
    public List<Associate> listerAssocies() throws SQLException, ParseException {
        ArrayList<Associate> listAssociates = new ArrayList<>();

        ResultSet resultSetAssociate = processingDB.getConnection().createStatement()
                .executeQuery("SELECT associate.id, `nom complet`, avg(DATEDIFF(prets.date_retour, prets.date_emprunt)) " +
                                        "AS DureeMoy " +
                                        "FROM prets \n" +
                                        "inner join associate\n" +
                                        "on prets.Associate =associate.id\n" +
                                        "group by Associate;");


        while(resultSetAssociate.next()) {
            listAssociates.add(new Associate(resultSetAssociate.getInt("id"),
                    resultSetAssociate.getString("nom complet"),
                    resultSetAssociate.getDouble("DureeMoy")));
        }



        return listAssociates;

    }
}
