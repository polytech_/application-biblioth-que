package BiblioMVC.Controller;

/**
 * Sample Skeleton for 'listeLivres.fxml' Controller Class
 */

import BiblioMVC.DAO.Livre.LivreDAO;
import BiblioMVC.DAO.ProcessingDB;
import BiblioMVC.Model.Livre;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ControllerLivres {

    private MainController mainController;

    private LivreDAO livreDAO;

    @FXML // fx:id="listLivreTV"
    private TableView<Livre> tableLivres; // Value injected by FXMLLoader

    @FXML // fx:id="ISBNCol"
    private TableColumn<Livre, String> ISBNCol; // Value injected by FXMLLoader

    @FXML // fx:id="CopiesCol"
    private TableColumn<Livre, String> CopiesCol; // Value injected by FXMLLoader

    @FXML // fx:id="TitreCol"
    private TableColumn<Livre, String> TitreCol; // Value injected by FXMLLoader

    @FXML // fx:id="PretsCol"
    private TableColumn<Livre, String> PretsCol; // Value injected by FXMLLoader

    @FXML // fx:id="ExemplairesCol"
    private TableColumn<Livre, String> ExemplairesCol; // Value injected by FXMLLoader





    public ControllerLivres() {
        ProcessingDB processingDB = ProcessingDB.getInstance();
        this.livreDAO = processingDB.getLivreDAO();
    }

    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }


    public void initialize() throws SQLException {

        ObservableList<Livre> observableListLivres = FXCollections.observableArrayList(livreDAO.listerLivres());

        ISBNCol.setCellValueFactory(new PropertyValueFactory<>("ISBN"));
        CopiesCol.setCellValueFactory(new PropertyValueFactory<>("Copies"));
        TitreCol.setCellValueFactory(new PropertyValueFactory<>("Titre"));
        PretsCol.setCellValueFactory(new PropertyValueFactory<>("Prets"));
        ExemplairesCol.setCellValueFactory(new PropertyValueFactory<>("Exemplaires"));

        tableLivres.setItems(observableListLivres);

    }















}

