package BiblioMVC.Controller;

import java.sql.*;

import BiblioMVC.DAO.Associe.AssociateDAO;
import BiblioMVC.DAO.Livre.LivreDAO;
import BiblioMVC.Model.Associate;
import BiblioMVC.Model.Livre;
import BiblioMVC.Model.Pret;
import BiblioMVC.DAO.Pret.PretDAO;
import BiblioMVC.DAO.ProcessingDB;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.text.ParseException;

public class MainController {

    private static Connection cnx;
    private ControllerLivres controllerLivres;
    private PretDAO pretDAO;
    private LivreDAO livreDAO;
    private AssociateDAO associateDAO;


    @FXML // fx:id="titreInput"
    private TextField titreInput; // Value injected by FXMLLoader

    @FXML // fx:id="exemplairesInput"
    private TextField exemplairesInput; // Value injected by FXMLLoader

    @FXML // fx:id="copiesInput"
    private TextField copiesInput; // Value injected by FXMLLoader


    @FXML // fx:id="displayCatalogue"
    private Button displayCatalogue; // Value injected by FXMLLoader

    @FXML // fx:id="ajoutBtn"
    private MenuButton ajoutBtn; // Value injected by FXMLLoader

    @FXML // fx:id="ajoutExemplaire"
    private MenuItem ajoutExemplaire; // Value injected by FXMLLoader

    @FXML // fx:id="ajoutCopie"
    private MenuItem ajoutCopie; // Value injected by FXMLLoader


    @FXML // fx:id="tablePrets"
    private TableView<Pret> tablePrets; // Value injected by FXMLLoader

    @FXML // fx:id="columLivre"
    private TableColumn<Pret, String> columLivre; // Value injected by FXMLLoader

    @FXML // fx:id="columnAssocie"
    private TableColumn<Pret, String> columnAssocie; // Value injected by FXMLLoader

    @FXML // fx:id="columnDateEmp"
    private TableColumn<Pret, String> columnDateEmp; // Value injected by FXMLLoader

    @FXML // fx:id="columnDateRetour"
    private TableColumn<Pret, String> columnDateRetour; // Value injected by FXMLLoader


    @FXML // fx:id="tableAssociate"
    private TableView<Associate> tableAssociate; // Value injected by FXMLLoader

    @FXML // fx:id="columIdAssociate"
    private TableColumn<Associate, String> columIdAssociate; // Value injected by FXMLLoader

    @FXML // fx:id="columnNameAssociate"
    private TableColumn<Associate, String> columnNameAssociate; // Value injected by FXMLLoader

    @FXML // fx:id="columnDureeMoy"
    private TableColumn<Associate, String> columnDureeMoy; // Value injected by FXMLLoader


    public MainController() {
        ProcessingDB processingDB = ProcessingDB.getInstance();
        this.pretDAO = processingDB.getPretDAO();
        this.livreDAO = processingDB.getLivreDAO();
        this.associateDAO = processingDB.getAssociateDAO();
        this.controllerLivres = new ControllerLivres();
    }

    public void setControllerLivres(ControllerLivres controllerLivres) {
        this.controllerLivres = controllerLivres;
    }

    public void initialize() throws SQLException, ParseException {

        controllerLivres.setMainController(this);

        //Remplissage table des prets
        ObservableList<Pret> observableListPrets = FXCollections.observableArrayList(pretDAO.listerPrets());

        columLivre.setCellValueFactory(new PropertyValueFactory<>("Livre"));
        columnAssocie.setCellValueFactory(new PropertyValueFactory<>("Associate"));
        columnDateEmp.setCellValueFactory(new PropertyValueFactory<>("date_emprunt"));
        columnDateRetour.setCellValueFactory(new PropertyValueFactory<>("date_retour"));

        tablePrets.setItems(observableListPrets);


        //Remplissage table associés
        ObservableList<Associate> observableListAssociates = FXCollections.observableArrayList(associateDAO.listerAssocies());

        columIdAssociate.setCellValueFactory(new PropertyValueFactory<>("Id"));
        columnNameAssociate.setCellValueFactory(new PropertyValueFactory<>("NomComplet"));
        columnDureeMoy.setCellValueFactory(new PropertyValueFactory<>("DureeMoy"));

        tableAssociate.setItems(observableListAssociates);

    }


    @FXML
    void handleActionAfficherLivres(ActionEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/listeLivres.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(root1));
            stage.setTitle("Liste des livres disponibles (exemplaires et copies)");
            stage.setResizable(false);
            stage.show();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }


    @FXML
    void ajoutCopie(ActionEvent event) throws SQLException {

        int copies = 0;
        int exemplaires = 0;
        String titre = titreInput.getText();
        String copiesString = copiesInput.getText();
        String exemplairesString = exemplairesInput.getText();

        //S'il n'y a pas de titre entré => renvoi d'erreur
        if(titre.equals("")) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur !");
            alert.setContentText("Merci d'entrer un titre !");
            alert.showAndWait();
        }

        if(!copiesString.matches("^[1-9]+$") || !exemplairesString.matches("^[1-9]+$")) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur !");
            alert.setContentText("Merci d'entrer une valeur numérique positive " +
                    "pour le nombre de copies et/ou le nombre d'exemplaires !");
            alert.showAndWait();
        }
        else {
            copies = Integer.parseInt(copiesString);
            exemplaires = Integer.parseInt(exemplairesString);


            if(copies<exemplaires) {
                //Stock < dispo => impossible!
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Erreur !");
                alert.setContentText("Merci d'entrer un nombre de copies inférieur au nombre d'exemplaires !");
                alert.showAndWait();
            }
            else {

                Livre livre = new Livre();
                livre.setTitre(titre);
                livre.setCopies(copies);
                livre.setExemplaires(exemplaires);

                livreDAO.ajouterLivre(livre);

                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Copie ajoutée !");
                alert.setContentText("Ajout de la copie réussi !");
                alert.show();

            }

        }


    }
    @FXML
    void ajoutExemplaire(ActionEvent event) throws SQLException {

        int copies = 0;
        int exemplaires = 0;
        String titre = titreInput.getText();
        String copiesString = copiesInput.getText();
        String exemplairesString = exemplairesInput.getText();

        //S'il n'y a pas de titre entré => renvoi d'erreur
        if(titre.equals("")) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur !");
            alert.setContentText("Merci d'entrer un titre !");
            alert.showAndWait();
        }

        //Si la valeur entrée n'est pas une valeur numérique positive pour les copies et les exemplaires => renvoi d'erreur
        if(!exemplairesString.matches("^[1-9]+$") || !copiesString.matches("^[1-9]+$")) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur !");
            alert.setContentText("Merci d'entrer une valeur numérique positive pour le nombre de copies et/ou le nombre d'exemplaires !");
            alert.showAndWait();
        }
        else {
            copies = Integer.parseInt(copiesString);
            exemplaires = Integer.parseInt(exemplairesString);

            if(copies<exemplaires) {
                //Stock < dispo => impossible!
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Erreur !");
                alert.setContentText("Merci d'entrer un nombre de copies inférieur au nombre d'exemplaires !");
                alert.showAndWait();
            }
            else {
                Livre livre = new Livre();
                livre.setTitre(titre);
                livre.setCopies(copies);
                livre.setExemplaires(exemplaires);

                livreDAO.ajouterLivre(livre);

                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Copie ajoutée !");
                alert.setContentText("Ajout de la copie réussi !");
                alert.show();

            }

        }



    }

}















